# Charts
Drawing charts with Javascript.

- Minimal without a framework.
- Pull data from REST api.
- Draw a graph of the data.

Testing tech by drawing inflation to median wages.

## Deploy
https://darfey.com/charts/index.html

### TODO
To get a cleaner address:

- Need to setup Apache proxy for it or
- Change the VPS to use Docker containers with NGinx proxy

Javascript is not minified.

## Interesting things
- retail and food prices from 1920s - 2020s
   - there is historic data available (needs to be copy pasted...)
- inflation
- crude oil, gold
- occupation by the year in the us
- housing prices
- debt levels (countries, consumers)
- wages
- wage cap
- suicide
- demographics
- unemployment vs. tax rate

## Population demographics in Finland
- immigration
- children, adults, elderly
- number of people living in cities compared to small towns

## True tax rate calculator
- Include the price payed pay the employer
- Include the price on consumer goods
- Include excess taxes: for example alcohol, cars and fuels

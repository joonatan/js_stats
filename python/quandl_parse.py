import json

filename = 'cpi_usa.json'
with open(filename, 'r') as f:
    full_json = json.loads(f.read())
    data = full_json['dataset']
    print('start date: {}'.format(data['start_date']))
    print('end date: {}'.format(data['end_date']))
    vals = data['data']
    # TODO draw the graph

import requests
import json

API_KEY="C-y29wvMy58zytvVUqEH"

# From 1915 to 1970
CPI_USA="FRED/M04128USM350NNBR"
# From 1960 to 2015
CPI_USA2="FRED/USACPIALLMINMEI"

API_URL="https://www.quandl.com/api/v3/datasets/{}.json".format(CPI_USA)
#?start_date=1985-05-01&end_date=1997-07-01&order=asc&column_index=4&collapse=quarterly&transformation=rdiff

print('sending request to {}'.format(API_URL))
p = requests.get(API_URL)
print(p)
# TODO check for 404
json_data = json.loads(p.text)
#print(json_data)

filename = 'cpi_usa.json'
with open(filename, 'w') as f:
    f.write (json.dumps(json_data, indent=2))

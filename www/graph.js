// TODO mobile version doesn't work since we hard-coded the size
// TODO Y-axis legand has a weird cuttoff
// TODO axis text colour needs to be more pronounced
// TODO calculate the real inflation
// TODO is there data for 2015-2019?
// TODO can we add data before 1955

// FIXME do something about the fill for when we have no data (don't render 0s?)

// median wages in the US: FRED/LES1252881600Q
// from 1979 to 2019
read_json = () => {
    const cpi_url="https://www.quandl.com/api/v3/datasets/FRED/USACPIALLMINMEI.json"
    const wages_url="https://www.quandl.com/api/v3/datasets/FRED/LES1252881600Q.json"
    $.getJSON(cpi_url, (cpi_data) => {
        //console.log(cpi_data)
        console.log('CPI dataset: ', cpi_data.dataset.name)

        $.getJSON(wages_url, (wages_data) => {
            //console.log(wages_data)
            console.log('Wages dataset: ', wages_data.dataset.name)

            const cpi = cpi_data.dataset

            const wages = _.reverse(
                wages_data.dataset.data
                    .filter(x => Number(x[0].substring(0, 4)) < 2015)
                    .map(x => Number(x[1]))
                )
            // TODO filling an extra three elements at the start,
            // not a good idea it's both hard-coded and in the wrong place
            //console.log('wages: ', wages)
            const w_val = [ ...Array((1979-1955)*12+3).fill(0),
                ...wages.reduce((acc, x) => [ ...acc, x, x, x ], [])
            ]
            //console.log(w_val)

            // TODO hard coded adjustment, we should use the closest max for cpi
            // now the wages are adjusted to 3-4 (inflation is at max 10)
            w_val_adj = w_val.map(x => x /100)

            const labels = cpi.data.map(x => x[0].split('-')[0])
            const reference = Number(cpi.data[cpi.data.length -1][1])
            //console.log(reference)
            const vals = cpi.data.map(x => Number(x[1]) / reference)
            //console.log(labels)
            //console.log(vals)
            var points = {
                labels: _.reverse(labels),
                series: [_.reverse(vals), w_val_adj]
            }

            var options = {
              showLine: false,
              // for mobile
              axisX: {
                labelInterpolationFnc: function(value, index) {
                    return (index % (4*10*12)) === 0 ? value : null;
                }
              },
             chartPadding: {
                top: 20,
                right: 0,
                bottom: 30,
                left: 0
              },
              plugins: [
                Chartist.plugins.ctAxisTitle({
                  axisX: {
                    axisTitle: 'Year',
                    axisClass: 'ct-axis-title',
                    offset: {
                      x: 0,
                      y: 50
                    },
                    textAnchor: 'middle'
                  },
                  axisY: {
                    axisTitle: 'Wages vs Inflation',
                    axisClass: 'ct-axis-title',
                    offset: {
                      x: 0,
                      y: 0
                    },
                    textAnchor: 'middle',
                    flipTitle: false
                  }
                })
              ]
            };

            var responsiveOptions = [
              // for desktops
              ['screen and (min-width: 640px)', {
                axisX: {
                  labelInterpolationFnc: function(value, index) {
                    return (index % (5*12)) === 0 ? value : null;
                  }
                }
              }]
            ];


            var chart = new Chartist.Line('.ct-chart', points, options, responsiveOptions)
        })
    })
}

read_json()
